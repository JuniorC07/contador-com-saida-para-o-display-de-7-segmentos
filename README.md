OBJETIVO
Criar um projeto para escrever nos displays de 7 segmentos, o qual deverá mostrar
os seguintes n°s e letras:
● Displays HEX0 e HEX1: contagem de 0 a 31;
● Displays HEX5 e HEX7: primeira letra do sobrenome e nome, respectivamente.
